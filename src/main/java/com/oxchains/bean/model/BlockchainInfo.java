package com.oxchains.bean.model;

import com.oxchains.common.BaseEntity;

public class BlockchainInfo extends BaseEntity{
    private String currentHashCode;

    private long blockHeight;

    private String preHashCode;

    private long blockSize;

    public void setCurrentHashCode(String currentHashCode) {
        this.currentHashCode = currentHashCode;
    }

    public void setBlockHeight(long blockHeight) {
        this.blockHeight = blockHeight;
    }

    public void setBlockSize(long blockSize) {
        this.blockSize = blockSize;
    }

    public void setPreHashCode(String preHashCode) {
        this.preHashCode = preHashCode;
    }

    public String getCurrentHashCode() {
        return currentHashCode;
    }

    public long getBlockHeight() {
        return blockHeight;
    }

    public long getBlockSize() {
        return blockSize;
    }

    public String getPreHashCode() {
        return preHashCode;
    }
}
