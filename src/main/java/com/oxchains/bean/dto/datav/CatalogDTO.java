package com.oxchains.bean.dto.datav;

import com.oxchains.bean.model.Catalog;
import com.oxchains.bean.model.Organization;
import com.oxchains.common.BaseEntity;

public class CatalogDTO extends BaseEntity{
    private Organization organization;

    private Catalog catalog;

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public Organization getOrganization() {
        return organization;
    }
}
