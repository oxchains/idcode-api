package com.oxchains.service;

import com.oxchains.bean.model.UserCode;
import com.oxchains.common.RespDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class UserCodeService extends BaseService{
    @Resource
    private FabricService fabricService;

    public RespDTO<String> addUserCode(UserCode userCode) throws Exception{
        fabricService.invoke("addUserCode", new String[] { gson.toJson(userCode) });
        return RespDTO.success();
    }
}
