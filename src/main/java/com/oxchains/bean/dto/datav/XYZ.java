package com.oxchains.bean.dto.datav;

import com.oxchains.common.BaseEntity;
import lombok.Data;

@Data
public class XYZ extends BaseEntity{
    private String x;
    private String y;
    private String z;

    public XYZ(){

    }

    public XYZ(String x, String y, String z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
