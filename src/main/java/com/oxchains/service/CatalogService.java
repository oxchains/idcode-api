package com.oxchains.service;

import com.oxchains.bean.model.Catalog;
import com.oxchains.common.RespDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
@Slf4j
public class CatalogService extends BaseService {

    @Resource
    private FabricService fabricService;

    public RespDTO<String> addCatalog(Catalog catalog) throws Exception{
        fabricService.invoke("addCatalog", new String[] { gson.toJson(catalog) });
        return RespDTO.success();
    }
}
