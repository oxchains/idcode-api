package com.oxchains.service;

import com.oxchains.bean.dto.datav.CatalogDTO;
import com.oxchains.bean.dto.datav.UserCodeDTO;
import com.oxchains.bean.model.BlockchainInfo;
import com.oxchains.bean.model.Catalog;
import com.oxchains.bean.model.Organization;
import com.oxchains.bean.model.UserCode;
import com.oxchains.common.BaseEntity;
import com.oxchains.common.ConstantsData;
import com.oxchains.common.RespDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Service
@Slf4j
public class IdcodeService extends BaseService {
    @Resource
    private FabricService fabricService;

    public RespDTO<BaseEntity> queryByIdcode(String idcode) throws Exception{
        String jsonStr = fabricService.query("queryByIdcode", new String[] { idcode });
        log.info("===queryByIdcode1===" + jsonStr);
        if (StringUtils.isEmpty(jsonStr) || "null".equals(jsonStr)) {
            return RespDTO.fail(ConstantsData.NO_DATA);
        }
        BlockchainInfo blockchainInfo = new BlockchainInfo();

        String[] arrIdcodes = idcode.split("/");

        if(arrIdcodes.length == 1){//单位信息
            Organization organization = simpleGson.fromJson(jsonStr, Organization.class);

            blockchainInfo = fabricService.queryBlockchainInfoByTxid(organization.getTxId());

            return RespDTO.success(organization, blockchainInfo);

        } else if(arrIdcodes.length == 2){// 品类码信息
            Catalog catalog = simpleGson.fromJson(jsonStr, Catalog.class);

            blockchainInfo = fabricService.queryBlockchainInfoByTxid(catalog.getTxId());

            jsonStr = fabricService.query("queryByIdcode", new String[] { catalog.getMainCode() });
            log.info("===queryByIdcode2===" + jsonStr);
            if (StringUtils.isEmpty(jsonStr) || "null".equals(jsonStr)) {
                return RespDTO.fail(ConstantsData.NO_DATA);
            }

            Organization organization = simpleGson.fromJson(jsonStr, Organization.class);

            CatalogDTO catalogDTO = new CatalogDTO();
            catalogDTO.setCatalog(catalog);
            catalogDTO.setOrganization(organization);

            return RespDTO.success(catalogDTO, blockchainInfo);

        } else if(arrIdcodes.length == 3){// 一物一码信息
            UserCode userCode = simpleGson.fromJson(jsonStr, UserCode.class);

            blockchainInfo = fabricService.queryBlockchainInfoByTxid(userCode.getTxId());

            jsonStr = fabricService.query("queryByIdcode", new String[] { userCode.getMainCode() +"/" + userCode.getCatalogCode() });
            log.info("===queryByIdcode3===" + jsonStr);
            if (StringUtils.isEmpty(jsonStr) || "null".equals(jsonStr)) {
                return RespDTO.fail(ConstantsData.NO_DATA);
            }

            Catalog catalog = simpleGson.fromJson(jsonStr, Catalog.class);

            jsonStr = fabricService.query("queryByIdcode", new String[] { userCode.getMainCode() });
            if (StringUtils.isEmpty(jsonStr) || "null".equals(jsonStr)) {
                return RespDTO.fail(ConstantsData.NO_DATA);
            }
            Organization organization = simpleGson.fromJson(jsonStr, Organization.class);


            UserCodeDTO userCodeDTO = new UserCodeDTO();
            userCodeDTO.setOrganization(organization);
            userCodeDTO.setCatalog(catalog);
            userCodeDTO.setUsercode(idcode);
            userCodeDTO.setTxId(userCode.getTxId());

            return RespDTO.success(userCodeDTO, blockchainInfo);

        } else {
            return RespDTO.fail(ConstantsData.IDCODE_IS_INVALID);
        }
    }
}
