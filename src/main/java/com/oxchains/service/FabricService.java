package com.oxchains.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.oxchains.bean.dto.datav.XYZ;
import com.oxchains.bean.model.Customer;
import com.oxchains.bean.model.TxInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.hyperledger.fabric.protos.common.Common;
import org.hyperledger.fabric.protos.common.Configtx;
import org.hyperledger.fabric.protos.msp.Identities;
import org.hyperledger.fabric.protos.peer.FabricTransaction;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.ChaincodeEndorsementPolicyParseException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.exception.TransactionException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.security.PrivateKey;
import java.security.Security;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@Slf4j
public class FabricService extends BaseService implements InitializingBean, DisposableBean {
    @Value("${chaincode.name}")
    private String CHAIN_CODE_NAME;

    @Value("${chaincode.path}")
    private String CHAIN_CODE_PATH;

    @Value("${chaincode.version}")
    private String CHAIN_CODE_VERSION;

    @Value("${chaincode.resource.path}")
    private String TEST_FIXTURES_PATH;

    @Value("${chaincode.ca.url}")
    private String CA_URL;

    @Value("${chaincode.orderer.url}")
    private String ORDERER_URL;

    @Value("${chaincode.peer.address.list}")
    private String PEER_LIST;

    @Value("${channel.config.path}")
    private String configPath;

    @Value("${channel.name}")
    private String channelName;

    @Value("${chaincode.privatekey}")
    private String PRIVATE_KEY;

    @Value("${chaincode.signcert}")
    private String SIGNCERT;

    @Value("${app.firststartup}")
    private String FIRST_STARTUP;

    private Channel channel;

    private HFClient hfClient;

    private HFCAClient hfcaClient;

    private ChaincodeID channelCodeID;

    private Customer peerOrgAdmin;

    private Customer customer;

    private Cache<String, BlockInfo> cache = CacheBuilder.newBuilder()
            .maximumSize(10)
            .expireAfterWrite(600, TimeUnit.SECONDS).build();


    public void installChaincode() throws InvalidArgumentException, ProposalException {
        InstallProposalRequest installProposalRequest = hfClient.newInstallProposalRequest();
        installProposalRequest.setChaincodeID(channelCodeID);
        installProposalRequest.setChaincodeSourceLocation(new File(TEST_FIXTURES_PATH));
        installProposalRequest.setChaincodeVersion(CHAIN_CODE_VERSION);

        Collection<ProposalResponse> responses = hfClient.sendInstallProposal(installProposalRequest, channel.getPeers());
        for (ProposalResponse response : responses) {
            if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
                log.info(String.format("Successful install proposal response Txid: %s from peer %s",
                        response.getTransactionID(),
                        response.getPeer().getName()));
            } else {
                log.info("install channelcode error!");
            }
        }
    }

    public void instantiateChaincode() throws IOException, ProposalException, InvalidArgumentException, InterruptedException, ExecutionException, TimeoutException, ChaincodeEndorsementPolicyParseException {

        //hfClient.setUserContext(customer);
        ChaincodeID channelCodeID = ChaincodeID.newBuilder().setName(CHAIN_CODE_NAME)
                .setVersion(CHAIN_CODE_VERSION)
                .setPath(CHAIN_CODE_PATH).build();
        InstantiateProposalRequest instantiateProposalRequest = hfClient.newInstantiationProposalRequest();
        instantiateProposalRequest.setChaincodeID(channelCodeID);
        instantiateProposalRequest.setFcn("init");
        instantiateProposalRequest.setArgs(new String[]{});
        Map<String, byte[]> transientMap = new HashMap<>();
        transientMap.put("HyperLedgerFabric", "InstantiateProposalRequest:JavaSDK".getBytes(UTF_8));
        transientMap.put("method", "InstantiateProposalRequest".getBytes(UTF_8));
        instantiateProposalRequest.setTransientMap(transientMap);

        ChaincodeEndorsementPolicy channelcodeEndorsementPolicy = new ChaincodeEndorsementPolicy();
        // 背书策略
        //channelcodeEndorsementPolicy.fromFile(new File(TEST_FIXTURES_PATH + "/members_from_org1_or_2.policy"));
        channelcodeEndorsementPolicy.fromYamlFile(new File(TEST_FIXTURES_PATH + "/channelcodeendorsementpolicy.yaml"));
        instantiateProposalRequest.setChaincodeEndorsementPolicy(channelcodeEndorsementPolicy);

        Collection<ProposalResponse> successful = new ArrayList<>();
        // Send instantiate transaction to peers
        Collection<ProposalResponse> responses = channel.sendInstantiationProposal(instantiateProposalRequest, channel.getPeers());
        if (responses != null && responses.size() > 0) {
            for (ProposalResponse response : responses) {
                if (response.isVerified() && response.getStatus() == ProposalResponse.Status.SUCCESS) {
                    successful.add(response);
                    log.info(String.format("Succesful instantiate proposal response Txid: %s from peer %s",
                            response.getTransactionID(),
                            response.getPeer().getName()));
                } else {
                    log.info("Instantiate Chaincode error! " + response.getMessage());
                }
            }

            /// Send instantiate transaction to orderer
            channel.sendTransaction(successful, channel.getOrderers());
            log.info("instantiateChaincode done");
        }
    }


    public Channel getChain(String channelName, Orderer orderer) throws InvalidArgumentException, TransactionException {
        Channel channel = hfClient.newChannel(channelName);

        Set<Peer> peers = getPeers();
        for (Peer peer : peers) {
            channel.addPeer(peer);
        }

        channel.addOrderer(orderer);
        channel.initialize();
        return channel;
    }

    public Channel createChain(String configPath, Orderer orderer, String channelName) throws IOException, InvalidArgumentException, TransactionException, ProposalException {
        ChannelConfiguration channelConfiguration = new ChannelConfiguration(new File(configPath));
        hfClient.setUserContext(peerOrgAdmin);
        Channel newChain = hfClient.newChannel(channelName, orderer, channelConfiguration, hfClient.getChannelConfigurationSignature(channelConfiguration, peerOrgAdmin));
        newChain.setTransactionWaitTime(100000);
        newChain.setDeployWaitTime(120000);

        Set<Peer> peers = getPeers();
        for (Peer peer : peers) {
            log.info("join channel: " + newChain.joinPeer(peer));
        }

        newChain.initialize();
        return newChain;
    }

    private Set<Peer> getPeers() throws InvalidArgumentException {
        Set<Peer> peers = new HashSet<>();
        String[] peerAddressList = PEER_LIST.split(",");
        for (String address : peerAddressList) {
            String[] params = address.split("@");
            peers.add(hfClient.newPeer(params[0], params[1]));
        }
        return peers;
    }

    public void invoke(String func, String[] args) throws InvalidArgumentException, ProposalException, InterruptedException, ExecutionException, TimeoutException {
        //String txID = null;

        TransactionProposalRequest transactionProposalRequest = hfClient.newTransactionProposalRequest();
        transactionProposalRequest.setChaincodeID(channelCodeID);
        transactionProposalRequest.setFcn(func);
        transactionProposalRequest.setArgs(args);

        // send Proposal to peers
        Collection<ProposalResponse> transactionPropResp = channel.sendTransactionProposal(transactionProposalRequest, channel.getPeers());

        // send Proposal to orderers
        Collection<ProposalResponse> successful = new ArrayList<>();
        for (ProposalResponse response : transactionPropResp) {
            if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
                //txID = response.getTransactionID();
                successful.add(response);
            }
        }
        channel.sendTransaction(successful, channel.getOrderers());
    }


    public String query(String func, String[] args) {
        QueryByChaincodeRequest queryByChaincodeRequest = hfClient.newQueryProposalRequest();
        queryByChaincodeRequest.setArgs(args);
        queryByChaincodeRequest.setFcn(func);
        queryByChaincodeRequest.setChaincodeID(channelCodeID);

        Collection<ProposalResponse> queryProposals = null;
        try {
            queryProposals = channel.queryByChaincode(queryByChaincodeRequest, channel.getPeers());
        } catch (InvalidArgumentException | ProposalException ignored) {
            log.error(ignored.getMessage());
            return null;
        }
        for (ProposalResponse proposalResponse : queryProposals) {
            if (proposalResponse.isVerified() && proposalResponse.getStatus() == ProposalResponse.Status.SUCCESS) {
                String payload = proposalResponse.getProposalResponse().getResponse().getPayload().toStringUtf8();
                log.debug("===payload==="+payload);
                return payload;
            }
        }

        return null;
    }



    public com.oxchains.bean.model.BlockchainInfo queryBlockchainInfoByHashcode(String hashcode) throws DecoderException, ProposalException, InvalidArgumentException {
        com.oxchains.bean.model.BlockchainInfo blockchainInfo = new com.oxchains.bean.model.BlockchainInfo();
        BlockInfo blockInfo =  channel.queryBlockByHash(Hex.decodeHex(hashcode.toCharArray()));
        if(blockInfo != null){
            blockchainInfo.setCurrentHashCode(hashcode);
            blockchainInfo.setBlockHeight(blockInfo.getBlockNumber());
            blockchainInfo.setPreHashCode(Hex.encodeHexString(blockInfo.getPreviousHash()));
            blockchainInfo.setBlockSize(blockInfo.getBlock().getSerializedSize());
        }
        else{
            log.error("queryBlockchainInfoByHashcode error: hashcode " + hashcode + " doesn't exist");
        }
        return blockchainInfo;
    }

    public com.oxchains.bean.model.BlockchainInfo queryBlockchainInfoByBlockNum(String blocknum) throws DecoderException, ProposalException, InvalidArgumentException, InvalidProtocolBufferException {
        com.oxchains.bean.model.BlockchainInfo blockchainInfo = new com.oxchains.bean.model.BlockchainInfo();
        BlockInfo blockInfo =  channel.queryBlockByNumber(Long.valueOf(blocknum));
        if(blockInfo != null){
            blockchainInfo.setCurrentHashCode(Hex.encodeHexString(blockInfo.getDataHash()));
            blockchainInfo.setBlockHeight(blockInfo.getBlockNumber());
            blockchainInfo.setPreHashCode(Hex.encodeHexString(blockInfo.getPreviousHash()));
            blockchainInfo.setBlockSize(blockInfo.getBlock().getSerializedSize());
            /*Iterator<BlockInfo.EnvelopeInfo> iterator = blockInfo.getTransactionActionInfos().iterator();
            while (iterator.hasNext()){
                BlockInfo.EnvelopeInfo envelopeInfo = iterator.next();
                System.out.println("timestamp: " + envelopeInfo.getTimestamp());
                System.out.println("txid: " + envelopeInfo.getTransactionID());
                System.out.println("channel: " + envelopeInfo.getChannelId());
            }
            for(ByteString byteString : blockInfo.getBlock().getData().getDataList()){
                final Common.Envelope envelope = Common.Envelope.parseFrom(byteString);
                final Common.Payload payload = Common.Payload.parseFrom(envelope.getPayload());
                //String str = new String(payload.getData().toByteArray());
                System.out.println("payload: " + payload.getData().toStringUtf8());
                System.out.println("==============================");
            }*/
        }
        else{
            log.error("queryBlockchainInfoByBlockNum error: blocknum " + blocknum + " doesn't exist");
        }
        return blockchainInfo;
    }


    public com.oxchains.bean.model.BlockchainInfo queryBlockchainInfoByTxid(String txId) throws ProposalException, InvalidArgumentException {
        com.oxchains.bean.model.BlockchainInfo blockchainInfo = new com.oxchains.bean.model.BlockchainInfo();

        BlockInfo blockInfo =  channel.queryBlockByTransactionID(txId);
        blockchainInfo.setCurrentHashCode(Hex.encodeHexString(blockInfo.getBlock().getHeader().getDataHash().toByteArray()));
        blockchainInfo.setBlockHeight(blockInfo.getBlockNumber());
        blockchainInfo.setPreHashCode(Hex.encodeHexString(blockInfo.getPreviousHash()));
        blockchainInfo.setBlockSize(blockInfo.getBlock().getSerializedSize());

        return blockchainInfo;
    }

    public String queryTransactionInfoByTxId(String txId) throws InvalidArgumentException, ProposalException, InvalidProtocolBufferException {
        TransactionInfo transactionInfo = channel.queryTransactionByID(txId);
        Common.Envelope envelope = transactionInfo.getEnvelope();
        //System.out.println("envelope: " + envelope.toByteString().toStringUtf8());
        /*Common.Payload p = Common.Payload.parseFrom(transactionInfo.getEnvelope().getPayload());
        String str = new String(p.getData().toByteArray());
        System.out.println("payload: " + str);*/
        String code = envelope.toByteString().toStringUtf8();
        if(code.indexOf("addOrganization") > -1){
            code = code.substring(code.indexOf("{\"loginName"), code.indexOf("}") + 1);
        }else if (code.indexOf("addCatalog") > -1){
            code = code.substring(code.indexOf("{\"mainCode"), code.indexOf("}") + 1);
        }else if (code.indexOf("addUserCode") > -1){
            code = code.substring(code.indexOf("{\"mainCode"), code.indexOf("}") + 1);
        }
        return code;
    }

    public List<TxInfo> queryTxRecentInfo(long txnum) throws InvalidArgumentException, ProposalException, InvalidProtocolBufferException {
        List<TxInfo> txInfoList = new ArrayList<>();
        BlockchainInfo blockchainInfo = channel.queryBlockchainInfo();
        if(blockchainInfo!=null && blockchainInfo.getHeight()>0){
            for (long i = blockchainInfo.getHeight() - 1; i >= 0; i--) {
                BlockInfo blockInfo = channel.queryBlockByNumber(i);
                Iterator<BlockInfo.EnvelopeInfo> iterator = blockInfo.getTransactionActionInfos().iterator();
                while (iterator.hasNext()){
                    if(txInfoList.size() > txnum - 1){
                        break;
                    }
                    TxInfo txInfo = new TxInfo();
                    BlockInfo.EnvelopeInfo envelopeInfo = iterator.next();
                    txInfo.setBlocknum(String.valueOf(i));
                    txInfo.setStatus("success");
                    txInfo.setTimestamp(envelopeInfo.getTimestamp().toString());
                    txInfo.setTxId(envelopeInfo.getTransactionID());
                    txInfo.setChannelId(envelopeInfo.getChannelId());
                    txInfoList.add(txInfo);
                }
            }
        }
        return txInfoList;
    }

    public List<XYZ> getBlockPerform(){
        List<XYZ> xyzList = new ArrayList<>();
        xyzList.add(new XYZ("5/9","290","260"));
        xyzList.add(new XYZ("5/10","330","270"));
        xyzList.add(new XYZ("5/11","300","220"));
        xyzList.add(new XYZ("5/12","461","369"));
        xyzList.add(new XYZ("5/13","290","199"));
        return xyzList;
    }

    public List<XYZ> getTxPerform(){
        List<XYZ> xyzList = new ArrayList<>();
        xyzList.add(new XYZ("5/9","1235","1000"));
        xyzList.add(new XYZ("5/10","1156","870"));
        xyzList.add(new XYZ("5/11","1123","980"));
        xyzList.add(new XYZ("5/12","1423","1020"));
        xyzList.add(new XYZ("5/13","1456","1200"));
        return xyzList;
    }

    public String getPeerInfo(){
        return "[\n" +
                "  {\n" +
                "    \"VP\": \"peer0\",\n" +
                "    \"IP\": \"10.104.199.173:7051\",\n" +
                "    \"TIME\": \"2018-04-18\",\n" +
                "    \"STATUS\": \"正常\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"VP\": \"peer1\",\n" +
                "    \"IP\": \"10.101.250.176:7051\",\n" +
                "    \"TIME\": \"2018-04-18\",\n" +
                "    \"STATUS\": \"正常\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"VP\": \"peer2\",\n" +
                "    \"IP\": \"10.107.47.158:7051\",\n" +
                "    \"TIME\": \"2018-04-18\",\n" +
                "    \"STATUS\": \"正常\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"VP\": \"peer3\",\n" +
                "    \"IP\": \"10.103.128.88:7051\",\n" +
                "    \"TIME\": \"2018-04-18\",\n" +
                "    \"STATUS\": \"正常\"\n" +
                "  }\n" +
                "]";
    }

    public BlockchainInfo queryChain() throws InvalidArgumentException, ProposalException, InvalidProtocolBufferException, ExecutionException {
        BlockchainInfo blockchannelInfo = channel.queryBlockchainInfo();
        /*String channelCurrentHash = Hex.encodeHexString(blockchannelInfo.getCurrentBlockHash());
        String channelPreviousHash = Hex.encodeHexString(blockchannelInfo.getPreviousBlockHash());*/

        /*System.out.println("height: " + blockchannelInfo.getHeight());
        System.out.println("currentHash: " + channelCurrentHash);
        System.out.println("previousHash: " + channelPreviousHash);*/

        log.info("size: {}.", blockchannelInfo.getBlockchainInfo().getSerializedSize());
        // TODO test
        for (int i = 0; i < blockchannelInfo.getHeight(); i++) {
            BlockInfo blockInfo = queryBlock(i);
            // block header
            Common.BlockHeader blockHeader = blockInfo.getBlock().getHeader();
            /*System.out.printf("dataHash: %s, previousHash: %s.\n",
                    Hex.encodeHexString(blockHeader.getDataHash().toByteArray()),
                    Hex.encodeHexString(blockHeader.getPreviousHash().toByteArray()));*/
            Common.BlockMetadata blockMetadata = blockInfo.getBlock().getMetadata();
            /*for (ByteString str : blockMetadata.getMetadataList()) {
                Common.Metadata metadata = Common.Metadata.parseFrom(str);
                System.out.println(metadata);
            }*/
            //System.out.println(blockMetadata);

            // block data
            Common.BlockData blockData = blockInfo.getBlock().getData();
            for (ByteString data : blockData.getDataList()) {
                // 获取txID
                Common.Envelope envelope = Common.Envelope.parseFrom(data);
                Common.Payload payload = Common.Payload.parseFrom(envelope.getPayload());
                Common.ChannelHeader channelHeader = Common.ChannelHeader.parseFrom(payload.getHeader().getChannelHeader());
                //System.out.println("txID: " + channelHeader.getTxId());
                //TransactionInfo transactionInfo = queryTransactionInfo(channelHeader.getTxId());

                if (channelHeader.getType() == 1) {
                    // 获取config
                    Configtx.ConfigEnvelope configEnvelope = Configtx.ConfigEnvelope.parseFrom(payload.getData());
                    Map<String, Configtx.ConfigGroup> map = configEnvelope.getConfig()
                            .getChannelGroup()
                            .getGroupsMap();

                    // decode ConfigGroup
                    parseConfigGroup(map);
                } else if (channelHeader.getType() == 3) {
                    // 获取transaction
                    FabricTransaction.Transaction transaction = FabricTransaction.Transaction.parseFrom(payload.getData());
                    for (FabricTransaction.TransactionAction transactionAction : transaction.getActionsList()) {
                        FabricTransaction.ChaincodeActionPayload channelcodeActionPayload = FabricTransaction.ChaincodeActionPayload.parseFrom(transactionAction.getPayload());
                        channelcodeActionPayload
                                .getAction()
                                .getEndorsementsList()
                                .forEach(endorsement -> {
                                    try {
                                        Identities.SerializedIdentity endorser = Identities.SerializedIdentity.parseFrom(endorsement.getEndorser());
                                        //System.out.println(String.format("mspID: %s, idByte: %s.", endorser.getMspid(), endorser.getIdBytes().toStringUtf8()));
                                    } catch (InvalidProtocolBufferException e) {
                                        e.printStackTrace();
                                    }
                                    //System.out.println();
                                });
                        //System.out.println();
                    }
                } else {
                    throw new RuntimeException("Only able to decode ENDORSER_TRANSACTION and CONFIG type blocks");
                }
                //System.out.println();
            }
            //System.out.println("height: " + i + ", blockData count: " + blockData.getDataCount());
        }
        //System.out.println();

        /*System.out.println("==================================");
        TransactionInfo transactionInfo = queryTransactionInfo("d2433a1e17e542bf865cbe2d3dd952d6c3f4a66f46476d86622313d6fcefbd3d");
        System.out.println(transactionInfo.getEnvelope());*/
        return blockchannelInfo;
    }

    private void parseConfigGroup(Map<String, Configtx.ConfigGroup> map) {
        Set<Map.Entry<String, Configtx.ConfigGroup>> entrySet = map.entrySet();
        for (Map.Entry<String, Configtx.ConfigGroup> entry : entrySet) {
            String key = entry.getKey();
            Configtx.ConfigGroup configGroup = entry.getValue();
            long version = configGroup.getVersion();
            // TODO groups 这是一个递归的过程

            // values
            Map<String, Configtx.ConfigValue> valueMap = configGroup.getValuesMap();
            for (String valueKey : valueMap.keySet()) {
                Configtx.ConfigValue configValue = valueMap.get(valueKey);
            }
        }
    }

    public BlockInfo queryBlock(long blockNumber) throws ProposalException, InvalidArgumentException, ExecutionException {
        String key = "queryBlock_" + blockNumber;
        BlockInfo blockInfo = cache.get(key, () -> {
            BlockInfo blockInfo1 = channel.queryBlockByNumber(blockNumber);
            cache.put(key, blockInfo1);
            return blockInfo1;
        });
        /*String previousHash = Hex.encodeHexString(blockInfo.getPreviousHash());
        System.out.println("queryBlockByNumber returned correct block with blockNumber " + blockInfo.getBlockNumber()
                + " \n previous_hash: " + previousHash);*/
        return blockInfo;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            System.setProperty("org.hyperledger.fabric.sdk.proposal.wait.time","120000");
            initPeerAdmin();
            String username = "admin";
            String password = "adminpw";
            Set<String> roles = null;
            String account = null;
            String affiliation = "peerOrg1";
            String mspID = "Org1MSP";
            String ordererName = "orderer0";

            hfClient = HFClient.createNewInstance();
            hfClient.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());
            hfcaClient = HFCAClient.createNewInstance(CA_URL, null);
            hfcaClient.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());
            //首次启动需要创建channel，否则不需要。1=首次启动，0=否
            if("1".equals(FIRST_STARTUP)){
                Enrollment enrollment = hfcaClient.enroll(username, password);
                customer = new Customer(username, enrollment, roles, account, affiliation, mspID);
                hfClient.setUserContext(customer);
                Orderer orderer = hfClient.newOrderer(ordererName, ORDERER_URL, null);
                try {
                    channel = createChain(configPath, orderer, channelName);
                } catch (IOException | InvalidArgumentException | TransactionException | ProposalException e) {
                    log.warn("createChain error!", e);
                    orderer = hfClient.newOrderer(ordererName, ORDERER_URL, null);
                    channel = getChain(channelName, orderer);
                } catch (Exception e) {
                    log.error("createChain error!", e);
                }
            }
            else{
                hfClient.setUserContext(peerOrgAdmin);
                Orderer orderer = hfClient.newOrderer(ordererName, ORDERER_URL, null);
                channel = getChain(channelName, orderer);
            }

            channelCodeID = ChaincodeID.newBuilder().setName(CHAIN_CODE_NAME)
                    .setVersion(CHAIN_CODE_VERSION)
                    .setPath(CHAIN_CODE_PATH).build();
        } catch (Exception e) {
            log.error("FabricService init error!", e);
            System.exit(1);
        }
    }

    private void initPeerAdmin() throws IOException {
        peerOrgAdmin = new Customer();
        peerOrgAdmin.setName("peerAdmin");
        peerOrgAdmin.setAccount(null);
        peerOrgAdmin.setAffiliation(null);
        peerOrgAdmin.setMspID("Org1MSP");
        peerOrgAdmin.setRoles(null);

        String certificate = new String(IOUtils.toByteArray(new FileInputStream(TEST_FIXTURES_PATH + "/msp/signcerts/"+SIGNCERT)), "UTF-8");

        String privateKeyFile = TEST_FIXTURES_PATH + "/msp/keystore/"+PRIVATE_KEY;
        final PEMParser pemParser = new PEMParser(new StringReader(new String(IOUtils.toByteArray(new FileInputStream(privateKeyFile)))));

        PrivateKeyInfo pemPair = (PrivateKeyInfo) pemParser.readObject();

        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        PrivateKey privateKey = new JcaPEMKeyConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME).getPrivateKey(pemPair);

        peerOrgAdmin.setEnrollment(AdminEnrollment.createInstance(privateKey, certificate));
    }

    public HFClient getHfClient() {
        return hfClient;
    }

    public Orderer getOrderer() {
        return channel.getOrderers().iterator().next();
    }

    public Channel getChain() {
        return channel;
    }

    private static class AdminEnrollment implements Enrollment {
        private PrivateKey privateKey;

        private String certificate;

        private AdminEnrollment() {
        }

        private static AdminEnrollment createInstance(PrivateKey privateKey, String certificate) {
            AdminEnrollment adminEnrollment = new AdminEnrollment();
            adminEnrollment.privateKey = privateKey;
            adminEnrollment.certificate = certificate;
            return adminEnrollment;
        }

        @Override
        public PrivateKey getKey() {
            return privateKey;
        }

        @Override
        public String getCert() {
            return certificate;
        }
    }

    @Override
    public void destroy() throws Exception {
        // channel shutdown
        channel.shutdown(true);
    }
}
