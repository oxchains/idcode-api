package com.oxchains.bean.dto.datav;

import com.oxchains.bean.model.Catalog;
import com.oxchains.bean.model.Organization;
import com.oxchains.common.BaseEntity;

public class UserCodeDTO extends BaseEntity{
    private Organization organization;

    private Catalog catalog;

    private String usercode;

    private String txId;

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public Organization getOrganization() {
        return organization;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public String getUsercode() {
        return usercode;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}
