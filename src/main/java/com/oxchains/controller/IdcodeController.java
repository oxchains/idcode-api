package com.oxchains.controller;

import com.oxchains.bean.model.Organization;
import com.oxchains.common.BaseEntity;
import com.oxchains.common.ConstantsData;
import com.oxchains.common.RespDTO;
import com.oxchains.service.IdcodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/idcode")
@Slf4j
public class IdcodeController extends BaseController{
    @Resource
    private IdcodeService idcodeService;

    @GetMapping
    public RespDTO<BaseEntity> queryByIdcode(@RequestParam String idcode){
        try {
            log.info("===queryByIdcode===" + idcode);
            if(StringUtils.isEmpty(idcode)){
                return RespDTO.fail(ConstantsData.ARGS_IS_EMPTY);
            }
            return idcodeService.queryByIdcode(idcode);
        }
        catch (Exception e) {
            log.error("queryByIdcode error: ",e);
            return RespDTO.fail(ConstantsData.SERVER_INTERNAL_ERROR);
        }
    }
}
