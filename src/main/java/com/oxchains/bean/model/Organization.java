package com.oxchains.bean.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oxchains.common.BaseEntity;

public class Organization extends BaseEntity{

    private String loginName;

    private String organunitName;

    private String organunitEnName;

    private String organunitType;

    private String country;

    private String province;

    private String city;

    private String area;

    private String address;

    private String enaddress;

    private String email;

    private String url;

    private String organunitOwner;

    private String mainCode;

    private String description;

    private String remark;

    private String isValid;

    private String txId;

    public String getTxId() {
        return txId;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getOrganunitName() {
        return organunitName;
    }

    public String getOrganunitEnName() {
        return organunitEnName;
    }

    public String getOrganunitType() {
        return organunitType;
    }

    public String getCountry() {
        return country;
    }

    public String getProvince() {
        return province;
    }

    public String getCity() {
        return city;
    }

    public String getArea() {
        return area;
    }

    public String getAddress() {
        return address;
    }

    public String getEnaddress() {
        return enaddress;
    }

    public String getEmail() {
        return email;
    }

    public String getUrl() {
        return url;
    }

    public String getOrganunitOwner() {
        return organunitOwner;
    }

    public String getMainCode() {
        return mainCode;
    }

    public String getDescription() {
        return description;
    }

    public String getRemark() {
        return remark;
    }

    public String getIsValid() {
        return isValid;
    }
}
