package com.oxchains.bean.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oxchains.common.BaseEntity;

public class Catalog extends BaseEntity{

    private String mainCode;

    private String catalogCode;

    private String catalogName;

    private String url;

    private String name;

    private String model;

    private String description;

    public String getMainCode() {
        return mainCode;
    }

    private String txId;

    public String getTxId() {
        return txId;
    }

    public String getCatalogCode() {
        return catalogCode;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public String getDescription() {
        return description;
    }
}
