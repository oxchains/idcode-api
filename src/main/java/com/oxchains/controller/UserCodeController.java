package com.oxchains.controller;

import com.google.gson.JsonSyntaxException;
import com.oxchains.bean.model.UserCode;
import com.oxchains.common.ConstantsData;
import com.oxchains.common.RespDTO;
import com.oxchains.service.CertificateService;
import com.oxchains.service.UserCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.*;
import java.security.cert.CertificateException;

@RestController
@RequestMapping("/usercode")
@Slf4j
public class UserCodeController extends BaseController {
    @Resource
    private UserCodeService userCodeService;

    @Resource
    private CertificateService certificateService;

    @PostMapping
    public RespDTO<String> addUserCode(@RequestBody String body, @RequestParam String signature) {
        try {
            log.info("===addUserCode==="+body + "===signature===" + signature);
            UserCode userCode = gson.fromJson(body,UserCode.class);
            if(StringUtils.isEmpty(signature)){
                return RespDTO.fail(ConstantsData.SIG_IS_EMPTY);
            }
            if(!certificateService.verifySignature(body,signature)){
                return RespDTO.fail(ConstantsData.SIG_VERIFY_FAIL);
            }
            return userCodeService.addUserCode(userCode);
        } catch(JsonSyntaxException e){
            log.error("addUserCode error: ", e);
            return RespDTO.fail(ConstantsData.JSON_PARSE_ERROR);
        } catch (CertificateException | NoSuchAlgorithmException | UnrecoverableKeyException | InvalidKeyException | SignatureException | KeyStoreException e) {
            log.error("addUserCode error: ", e);
            return RespDTO.fail(ConstantsData.SIG_VERIFY_FAIL);
        } catch (Exception e) {
            log.error("addUserCode error: ", e);
            return RespDTO.fail(ConstantsData.SERVER_INTERNAL_ERROR);
        }
    }
}
