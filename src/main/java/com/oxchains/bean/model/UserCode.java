package com.oxchains.bean.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oxchains.common.BaseEntity;

public class UserCode extends BaseEntity {

    private String mainCode;

    private String catalogCode;

    private String userCodes;

    private long startVal;

    private long endVal;

    public String getMainCode() {
        return mainCode;
    }

    public String getCatalogCode() {
        return catalogCode;
    }

    public String getUserCodes() {
        return userCodes;
    }

    private String txId;

    public String getTxId() {
        return txId;
    }

    public long getStartVal() {
        return startVal;
    }

    public long getEndVal() {
        return endVal;
    }
}
