package com.oxchains.controller;

import com.oxchains.bean.model.BlockchainInfo;
import com.oxchains.common.ConstantsData;
import com.oxchains.common.RespDTO;
import com.oxchains.service.BlockService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/blockinfo")
@Slf4j
public class BlockController extends BaseController{

    @Resource
    private BlockService blockService;

    @GetMapping
    public RespDTO<List<BlockchainInfo>> getBlockinfoByHash(@RequestParam String hashCode) {
        try {
        	log.info("===getBlockinfoByHash==="+hashCode);
            if(StringUtils.isEmpty(hashCode)){
                return RespDTO.fail(ConstantsData.ARGS_IS_EMPTY);
            }
            BlockchainInfo blockchainInfo = blockService.queryBlockchainInfoByHashcode(hashCode);
            return RespDTO.success(null,blockchainInfo);
        }catch (DecoderException | InvalidArgumentException | ProposalException e) {
            log.error("getBlockinfoByHash error: ",e);
            return RespDTO.fail(ConstantsData.NO_SUCH_BLOCK);
        } catch (Exception e) {
            log.error("getBlockinfoByHash error: ",e);
            return RespDTO.fail(ConstantsData.SERVER_INTERNAL_ERROR);
        }
    }

    @GetMapping("/blocknum")
    public RespDTO<List<BlockchainInfo>> getBlockinfoByBlockNum(@RequestParam String blockNum) {
        try {
            log.info("===getBlockinfoByBlockNum==="+blockNum);
            if(StringUtils.isEmpty(blockNum)){
                return RespDTO.fail(ConstantsData.ARGS_IS_EMPTY);
            }
            BlockchainInfo blockchainInfo = blockService.queryBlockchainInfoByBlockNum(blockNum);
            return RespDTO.success(null,blockchainInfo);
        }catch (DecoderException | InvalidArgumentException | ProposalException e) {
            log.error("getBlockinfoByBlockNum error: ",e);
            return RespDTO.fail(ConstantsData.NO_SUCH_BLOCK);
        } catch (Exception e) {
            log.error("getBlockinfoByBlockNum error: ",e);
            return RespDTO.fail(ConstantsData.SERVER_INTERNAL_ERROR);
        }
    }

    @GetMapping("/txinfo")
    public RespDTO<String> getTxInfoByTxId(@RequestParam String txId) {
        try {
            log.info("===getTxInfoByTxId==="+txId);
            if(StringUtils.isEmpty(txId)){
                return RespDTO.fail(ConstantsData.ARGS_IS_EMPTY);
            }
            String payload = blockService.queryTxInfoByTxId(txId);
            return RespDTO.success(payload, null);
        }catch (InvalidArgumentException | ProposalException e) {
            log.error("getTxInfoByTxId error: ",e);
            return RespDTO.fail(ConstantsData.NO_SUCH_TX);
        } catch (Exception e) {
            log.error("getTxInfoByTxId error: ",e);
            return RespDTO.fail(ConstantsData.SERVER_INTERNAL_ERROR);
        }
    }
}
