/*
Copyright IBM Corp 2016 All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"time"
	"strings"
	"strconv"
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type myChaincode struct {
}


type Organization struct {
	LoginName         	string `json:"loginName"`
	OrganunitName 		string `json:"organunitName"`
	OrganunitEnName  	string `json:"organunitEnName"`
	OrganunitType       string `json:"organunitType"`
	Country				string `json:"country"`
	Province			string `json:"province"`
	City				string `json:"city"`
	Area				string `json:"area"`
	Address				string `json:"address"`
	Enaddress			string `json:"enaddress"`
	Email				string `json:"email"`
	Url				    string `json:"url"`
	OrganunitOwner		string `json:"organunitOwner"`
	MainCode			string `json:"mainCode"`
	Description			string `json:"description"`
	Remark				string `json:"remark"`
	IsValid				string `json:"isValid"`
	TxId				string `json:"txId"`
	TxTime				int64 `json:"txTime"`
}

type Catalog struct {
	MainCode		string `json:"mainCode"`
	CatalogCode		string `json:"catalogCode"`
	CatalogName		string `json:"catalogName"`
	Url				string `json:"url"`
	Name			string `json:"name"`
	Model			string `json:"model"`
	Description		string `json:"description"`
	TxId			string `json:"txId"`
	TxTime			int64 `json:"txTime"`
}

type UserCode struct {
	MainCode		string `json:"mainCode"`
	CatalogCode		string `json:"catalogCode"`
	UserCodes		string `json:"userCodes"`
	StartVal		int64  `json:"startVal"`
	EndVal			int64  `json:"endVal"`
	TxId			string `json:"txId"`
	TxTime			int64 `json:"txTime"`
}


// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(myChaincode))
	if err != nil {
		fmt.Printf("Error starting myChaincode chaincode: %s", err)
	}
}

// Init resets all the things
func (t *myChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke is our entry point to invoke a chaincode function
func (t *myChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	switch function {

	case "addOrganization":
		return t.addOrganization(stub, args)
	case "addCatalog":
		return t.addCatalog(stub, args)
	case "addUserCode":
		return t.addUserCode(stub, args)
	case "queryByIdcode":
		return t.queryByIdcode(stub, args)

	default:
		return shim.Error("Unsupported operation")
	}
}

func (t *myChaincode) addOrganization(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("===addOrganization===")
	if len(args) < 1 {
		return shim.Error("addOrganization operation must have 1 arg")
	}
	// get the args
	bOrganization := []byte(args[0])
	organization := &Organization{}
	err := json.Unmarshal(bOrganization, &organization)
	if err != nil {
		fmt.Println(err)
		return shim.Error("Unmarshal failed")
	}

	organization.TxId = stub.GetTxID()
	organization.TxTime = time.Now().Unix()
	dataInfo , err2 := json.Marshal(organization)
	if err2 != nil {
		fmt.Println(err2)
		return shim.Error("Marshal failed")
	}
	//save the json info
	err = stub.PutState(organization.MainCode, dataInfo)
	if err != nil {
		return shim.Error("putting state err: " + err.Error())
	}
	return shim.Success(nil)
}

func (t *myChaincode) addCatalog(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("===addCatalog===")
	if len(args) < 1 {
		return shim.Error("addCatalog operation must have 1 arg")
	}
	// get the args
	bCatalog := []byte(args[0])
	catalog := &Catalog{}
	err := json.Unmarshal(bCatalog, &catalog)
	if err != nil {
		fmt.Println(err)
		return shim.Error("Unmarshal failed")
	}

	catalog.TxId = stub.GetTxID()
	catalog.TxTime = time.Now().Unix()
	dataInfo , err2 := json.Marshal(catalog)
	if err2 != nil {
		fmt.Println(err2)
		return shim.Error("Marshal failed")
	}

	//save the json info
	err = stub.PutState(catalog.MainCode + "/" + catalog.CatalogCode, dataInfo)
	if err != nil {
		return shim.Error("putting state err: " + err.Error())
	}
	return shim.Success(nil)
}


func (t *myChaincode) addUserCode(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("===addUserCode===")
	fmt.Println("===starttime:",time.Now().Format("2006-01-02 15:04:05"))
	if len(args) < 1 {
		return shim.Error("addUserCode operation must have 1 arg")
	}
	// get the args
	bUserCode := []byte(args[0])
	usercode := &UserCode{}
	err := json.Unmarshal(bUserCode, &usercode)
	if err != nil {
		fmt.Println(err)
		return shim.Error("Unmarshal failed")
	}

	if usercode.UserCodes != "" {
		arrUsercodes := strings.Split(usercode.UserCodes, ",")
		usercode.TxId = stub.GetTxID()
		usercode.TxTime = time.Now().Unix()
		dataInfo , err2 := json.Marshal(usercode)
		if err2 != nil {
			fmt.Println(err2)
			return shim.Error("Marshal failed")
		}
		for _, v := range arrUsercodes {
			err = stub.PutState(usercode.MainCode + "/" + usercode.CatalogCode + "/" + v, dataInfo)
			if err != nil {
				return shim.Error("putting state err: " + err.Error())
			}
		}
	} else if (usercode.StartVal >= 0) && (usercode.StartVal <= usercode.EndVal) {
		usercode.TxId = stub.GetTxID()
		usercode.TxTime = time.Now().Unix()
		dataInfo , err2 := json.Marshal(usercode)
		if err2 != nil {
			fmt.Println(err2)
			return shim.Error("Marshal failed")
		}
		for usercode.StartVal <= usercode.EndVal {
			err = stub.PutState(usercode.MainCode + "/" + usercode.CatalogCode + "/" + strconv.FormatInt(usercode.StartVal,10), dataInfo)
			if err != nil {
				return shim.Error("putting state err: " + err.Error())
			}
			usercode.StartVal++
		}
	}
	fmt.Println("===endtime  :",time.Now().Format("2006-01-02 15:04:05"))
	return shim.Success(nil)
}

func (t *myChaincode) queryByIdcode(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) < 1 {
		return shim.Error("queryByIdcode operation must have 1 ars")
	}
	idcode := args[0]
	fmt.Println("===queryByIdcode===", idcode)
 	val, err := stub.GetState(idcode)
	if err != nil {
		return shim.Error("queryByIdcode operation failed while getting the state : " + err.Error())
	}

	return shim.Success(val)

}