package com.oxchains.service;


import com.oxchains.bean.model.Organization;
import com.oxchains.common.ConstantsData;
import com.oxchains.common.RespDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class OrganizationService extends BaseService {

    @Resource
    private FabricService fabricService;

    public RespDTO<String> addOrganization(Organization organization) throws Exception{
        fabricService.invoke("addOrganization", new String[] { gson.toJson(organization) });
        return RespDTO.success();
    }
}
