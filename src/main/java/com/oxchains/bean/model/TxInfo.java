package com.oxchains.bean.model;

import com.oxchains.common.BaseEntity;

public class TxInfo extends BaseEntity{
    private String txId;
    private String timestamp;
    private String blocknum;
    private String status;
    private String channelId;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTxId() {
        return txId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getBlocknum() {
        return blocknum;
    }

    public String getStatus() {
        return status;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setBlocknum(String blocknum) {
        this.blocknum = blocknum;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
