# idcode-api
## 用户注册
- POST /user/register  
- request parameters  
    {  
        "username":"String",  
        "password":"String"  
    }  
- request content type: application/json
- response  
    {  
        "status": "int, 0=成功, -1=失败",  
        "message": "String, 操作成功/失败",  
        "data": null,  
        "blockchainInfo": null  
    }
## 用户登录
- POST /user/login  
- request parameters  
    {  
        "username":"String",  
        "password":"String"  
    }  
- request content type: application/json
- response  
    {  
        "status": "int, 0=成功, -1=失败",  
        "message": "String, 操作成功/失败",  
        "data": "String, token",  
        "blockchainInfo": null  
    }
## 增加单位/组织信息
- POST /organization?token=String  
- request parameters  
    {  
        "loginName":"String",  
        "organunitName":"String",  
        "organunitEnName":"String",  
        "organunitType":"String",  
        "country":"String",  
        "province":"String",  
        "city":"String",  
        "area":"String",  
        "address":"String",  
        "enaddress":"String",  
        "email":"String",  
        "url":"String",  
        "organunitOwner":"String",  
        "mainCode":"String",  
        "description":"String",  
        "remark":"String",  
        "isValid":"String, 1=有效, 0=无效"  
    }  
- request content type: application/json
- response  
    {  
        "status": "int, 0=成功, -1=失败",  
        "message": "String, 操作成功/失败",  
        "data": null,  
        "blockchainInfo": null  
    }
## 增加catalog信息
- POST /catalog?token=String  
- request parameters  
    {  
    	"description":"String",  
    	"model":"String",  
    	"name":"String",  
    	"url":"String",  
    	"organunitOwner":"String",  
    	"mainCode":"String",
    	"catalogCode":"String",  
    	"catalogName":"String"  
    }  
- request content type: application/json
- response  
    {  
        "status": "int, 0=成功, -1=失败",  
        "message": "String, 操作成功/失败",  
        "data": null,  
        "blockchainInfo": null  
    }   
## 增加usercode信息
- POST /usercode?token=String  
- request parameters  
    {  
    	"mainCode":"String",  
    	"catalogCode":"String",  
    	"startVal":long,  
    	"endVal":long,  
    	"userCodes":"String, such as 1001,1002,1003"  
    }
- request content type: application/json
- response  
    {  
        "status": "int, 0=成功, -1=失败",  
        "message": "String, 操作成功/失败",  
        "data": null,  
        "blockchainInfo": null  
    }  
## 查询idcode信息
- GET /idcode?idcode=String&token=String  
- response  
    {  
        "status": "int, 0=成功, -1=失败",  
        "message": "String, 操作成功/失败",  
        "data": 
        {  
            "organization":{...},  
            "catalog":{...},  
            "usercode":"String"                  
        },  
        "blockchainInfo":  
        {
            "currentHashCode": "String",  
            "blockHeight": "long",  
            "preHashCode": "String",  
            "blockSize": "long"  
        }  
    }
## 查询block信息
- GET /blockinfo?hashCode=String&token=String
- response  
    {  
        "status": "int, 0=成功, -1=失败",  
        "message": "String, 操作成功/失败",  
        "data": null,  
        "blockchainInfo":  
        {  
            "currentHashCode": "String",  
            "blockHeight": "long",  
            "preHashCode": "String",  
            "blockSize": "long"  
        }  
    }