package com.oxchains.controller;


import com.google.gson.JsonSyntaxException;
import com.oxchains.bean.model.Catalog;
import com.oxchains.common.ConstantsData;
import com.oxchains.common.RespDTO;
import com.oxchains.service.CatalogService;
import com.oxchains.service.CertificateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.*;
import java.security.cert.CertificateException;

@RestController
@RequestMapping("/catalog")
@Slf4j
public class CatalogController extends BaseController{
    @Resource
    private CatalogService catalogService;

    @Resource
    private CertificateService certificateService;

    @PostMapping
    public RespDTO<String> addCatalog(@RequestBody String body, @RequestParam String signature) {
        try {
            log.info("===addCatalog==="+body+ "===signature===" + signature);
            Catalog catalog = gson.fromJson(body,Catalog.class);
            if(StringUtils.isEmpty(signature)){
                return RespDTO.fail(ConstantsData.SIG_IS_EMPTY);
            }
            if(!certificateService.verifySignature(body,signature)){
                return RespDTO.fail(ConstantsData.SIG_VERIFY_FAIL);
            }
            //TODO whether need to check mainCode exists?
            return catalogService.addCatalog(catalog);
        }catch(JsonSyntaxException e){
            log.error("addCatalog error: ", e);
            return RespDTO.fail(ConstantsData.JSON_PARSE_ERROR);
        }catch (CertificateException | NoSuchAlgorithmException | UnrecoverableKeyException | InvalidKeyException | SignatureException | KeyStoreException e) {
            log.error("addCatalog error: ", e);
            return RespDTO.fail(ConstantsData.SIG_VERIFY_FAIL);
        }catch (Exception e) {
            log.error("addCatalog error: ", e);
            return RespDTO.fail(ConstantsData.SERVER_INTERNAL_ERROR);
        }
    }

}
