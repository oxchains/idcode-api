package com.oxchains.service;

import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@Service
@Slf4j
public class CertificateService extends BaseService{

    @Value("${cert.key.store}")
    private String KEYSTORE_PATH;

    @Value("${cert.key.pass}")
    private String STORE_PASS;

    @Value("${cert.key.alias}")
    private String ALIAS;

    @Value("${cert.cert}")
    private String CERT_PATH;

    private String CERT_TYPE = "X.509";

    public static void main(String[] args) throws Exception {
        //例如增加catalog，json字符串为：{"description":"description","model":"model","name":"oxchains"}
        String jsonString = "{\n" +
                "\t\"loginName\":\"testuser\",\n" +
                "\t\"organunitName\":\"牛链科技\",\n" +
                "\t\"organunitEnName\":\"oxchains\",\n" +
                "\t\"organunitType\":\"民营企业\",\n" +
                "\t\"country\":\"中国\",\n" +
                "\t\"province\":\"北京\",\n" +
                "\t\"city\":\"北京\",\n" +
                "\t\"area\":\"海淀区\",\n" +
                "\t\"address\":\"中关村soho\",\n" +
                "\t\"enaddress\":\"ZGC soho\",\n" +
                "\t\"email\":\"oxchains@oxchains.com\",\n" +
                "\t\"url\":\"http://a.b.com/111/222\",\n" +
                "\t\"organunitOwner\":\"企业法人\",\n" +
                "\t\"mainCode\":\"i.86.1003.1001\",\n" +
                "\t\"description\":\"描述\",\n" +
                "\t\"remark\":\"备注\",\n" +
                "\t\"isValid\":\"1\"\n" +
                "}";

        CertificateService certificateService = new CertificateService();

        String signature = certificateService.getSignatureString(jsonString);

        System.out.println("plaintext :     " + jsonString);
        System.out.println("Signature is :  " + signature);
        System.out.println("verify result : " + certificateService.verifySignature(jsonString, signature));
    }

    //客户端生成签名
    public String getSignatureString(String jsonString) throws Exception {
        KeyStore keyStore = getKeyStore(STORE_PASS, KEYSTORE_PATH);
        PrivateKey privateKey = getPrivateKey(keyStore, ALIAS, STORE_PASS);
        X509Certificate certificate = getCertificateByKeystore(keyStore, ALIAS);
        byte[] signature = sign(certificate, privateKey, jsonString.getBytes("UTF-8"));
        String signatureHex = Hex.toHexString(signature).substring(0,16);//
        return signatureHex;
    }

    //服务器端验证签名
    public boolean verifySignature(String decodedText, String receivedsignature) throws IOException, CertificateException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, SignatureException, InvalidKeyException {
        X509Certificate certificate = getCertificateByCertPath(CERT_PATH, CERT_TYPE);
        KeyStore keyStore = getKeyStore(STORE_PASS, KEYSTORE_PATH);
        PrivateKey privateKey = getPrivateKey(keyStore, ALIAS, STORE_PASS);
        byte[] signature = sign(certificate, privateKey, decodedText.getBytes("UTF-8"));
        String signatureHex = Hex.toHexString(signature).substring(0,16);
        log.info("===verifySignature===receivedsignature===" + receivedsignature + "===signatureHex===" + signatureHex);
        if(!StringUtils.isEmpty(signatureHex) && signatureHex.equals(receivedsignature)){
            return true;
        }
        return false;
    }

    public KeyStore getKeyStore(String storepass, String keystorePath)
            throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        InputStream inputStream = null;
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        //inputStream = new FileInputStream(keystorePath);
        inputStream = new ClassPathResource(keystorePath).getInputStream();
        keyStore.load(inputStream, storepass.toCharArray());
        if (null != inputStream) {
            inputStream.close();
        }
        return keyStore;
    }

    public PrivateKey getPrivateKey(KeyStore keyStore, String alias,
                                           String password) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
        return (PrivateKey) keyStore.getKey(alias, password.toCharArray());
    }

    public X509Certificate getCertificateByKeystore(KeyStore keyStore,
                                                           String alias) throws KeyStoreException {
        return (X509Certificate) keyStore.getCertificate(alias);
    }

    public X509Certificate getCertificateByCertPath(String path,
                                                           String certType) throws IOException, CertificateException {
        InputStream inputStream = null;
        CertificateFactory factory = CertificateFactory
                .getInstance(certType);

        //inputStream = new FileInputStream(path);
        inputStream = new ClassPathResource(path).getInputStream();
        Certificate certificate = factory.generateCertificate(inputStream);
        if (null != inputStream) {
            inputStream.close();
        }
        return (X509Certificate) certificate;
    }

    public byte[] sign(X509Certificate certificate,
                              PrivateKey privateKey, byte[] plainText) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signature = Signature.getInstance(certificate
                .getSigAlgName());
        signature.initSign(privateKey);
        signature.update(plainText);
        return signature.sign();
    }



}
