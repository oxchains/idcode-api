package com.oxchains.controller;

import com.oxchains.bean.dto.datav.NameValue;
import com.oxchains.bean.dto.datav.ValueContent;
import com.oxchains.bean.dto.datav.XY;
import com.oxchains.bean.dto.datav.XYZ;
import com.oxchains.bean.model.TxInfo;
import com.oxchains.service.DataVService;
import com.oxchains.service.FabricService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/datav")
@Slf4j
public class DataVController extends BaseController {
    @Resource
    private DataVService dataVService;

    @Resource
    private FabricService fabricService;

    @RequestMapping(value = "/chain/height", method = RequestMethod.GET)
    public List<NameValue<Long>> chainHeight() {
        List<NameValue<Long>> list = new ArrayList<>(1);
        NameValue<Long> nameValue = null;
        try {
            nameValue = dataVService.getChainHeight();
        } catch (Exception e) {
            log.error("chainHeight error!", e);
            nameValue = new NameValue<>("", 1L);
        }
        list.add(nameValue);
        return list;
    }

    @RequestMapping(value = "/chain/txCount", method = RequestMethod.GET)
    public List<NameValue<Long>> chainTxCount() {
        List<NameValue<Long>> list = new ArrayList<>(1);
        NameValue<Long> nameValue = null;
        try {
            nameValue = dataVService.getChainTxCount();
        } catch (Exception e) {
            log.error("chainTxCount error!", e);
            nameValue = new NameValue<>("", 1L);
        }
        list.add(nameValue);
        return list;
    }

    @RequestMapping(value = "/chain/txNum", method = RequestMethod.GET)
    public List<XY> chainTxNum() {
        List<XY> list = null;
        try {
            list = dataVService.getChainTxNum();
        } catch (Exception e) {
            log.error("chainTxNum error!", e);
        }
        return list;
    }

    @RequestMapping(value = "/chain/new", method = RequestMethod.GET)
    public List<ValueContent> chainNewBlock() {
        List<ValueContent> list = null;
        try {
            list = dataVService.getChainNewBlock();
        } catch (Exception e) {
            log.error("chainNewBlock error!", e);
        }
        return list;
    }

    @GetMapping("/txrecentinfo")
    public List<TxInfo> getTxRecentInfo(@RequestParam(required = false, defaultValue = "10") long number) {
        try {
            log.info("===getTxRecentInfo===" + number);
            List<TxInfo> txInfoList = fabricService.queryTxRecentInfo(number);
            return txInfoList;
        } catch (Exception e) {
            log.error("getTxRecentInfo error: ", e);
            return null;
        }
    }

    @GetMapping("/blockperform")
    public List<XYZ> getBlockPerform(){
        try {
            log.info("===getBlockPerform===");
            List<XYZ> xyzList = fabricService.getBlockPerform();
            return xyzList;
        } catch (Exception e) {
            log.error("getBlockPerform error: ", e);
            return null;
        }
    }

    @GetMapping("/txperform")
    public List<XYZ> getTxPerform(){
        try {
            log.info("===getTxPerform===");
            List<XYZ> xyzList = fabricService.getTxPerform();
            return xyzList;
        } catch (Exception e) {
            log.error("getTxPerform error: ", e);
            return null;
        }
    }

    @GetMapping("/peerinfo")
    public String getPeerInfo(){
        try {
            log.info("===getPeerInfo===");
            String peerInfo = fabricService.getPeerInfo();
            return peerInfo;
        } catch (Exception e) {
            log.error("getPeerInfo error: ", e);
            return null;
        }
    }
}
