package com.oxchains.controller;

import com.google.gson.JsonSyntaxException;
import com.oxchains.bean.model.Organization;
import com.oxchains.common.ConstantsData;
import com.oxchains.common.RespDTO;
import com.oxchains.service.CertificateService;
import com.oxchains.service.OrganizationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.*;
import java.security.cert.CertificateException;

@RestController
@RequestMapping("/organization")
@Slf4j
public class OrganizationController extends BaseController {
    @Resource
    private OrganizationService organizationService;

    @Resource
    private CertificateService certificateService;

    @PostMapping
    public RespDTO<String> addOrganization(@RequestBody String body, @RequestParam String signature) {
        try {
            log.info("===addOrganization==="+body + "===signature===" + signature);
            Organization organization = gson.fromJson(body,Organization.class);
            if(StringUtils.isEmpty(signature)){
                return RespDTO.fail(ConstantsData.SIG_IS_EMPTY);
            }
            if(!certificateService.verifySignature(body,signature)){
                return RespDTO.fail(ConstantsData.SIG_VERIFY_FAIL);
            }
            return organizationService.addOrganization(organization);
        } catch(JsonSyntaxException e){
            log.error("addOrganization error: ", e);
            return RespDTO.fail(ConstantsData.JSON_PARSE_ERROR);
        } catch (CertificateException | NoSuchAlgorithmException | UnrecoverableKeyException | InvalidKeyException | SignatureException | KeyStoreException e) {
            log.error("addOrganization error: ", e);
            return RespDTO.fail(ConstantsData.SIG_VERIFY_FAIL);
        } catch (Exception e) {
            log.error("addOrganization error: ", e);
            return RespDTO.fail(ConstantsData.SERVER_INTERNAL_ERROR);
        }
    }
}
