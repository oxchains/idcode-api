package com.oxchains.common;

public class ConstantsData {
	/**
	 * 返回码定义值
	 */
	public static final String SERVER_INTERNAL_ERROR = "服务器内部错误";

	public static final String NO_DATA = "没有数据";

	public static final String ARGS_IS_EMPTY = "参数值不能为空";

	public static final String IDCODE_IS_INVALID = "idcode编码格式非法";

	public static final String RTN_UNLOGIN = "用户未登录";

	public static final String RTN_UNREGISTER = "用户未注册";

	public static final String RTN_LOGIN_EXPIRED = "登录超时";

	public static final String RTN_DATA_ALREADY_EXISTS = "用户已经存在";

	public static final String RTN_USERNAMEORPWD_ERROR = "用户名或密码错误";

	public static final String SIG_IS_EMPTY = "签名不能为空";

	public static final String SIG_VERIFY_FAIL = "签名校验失败";

	public static final String JSON_PARSE_ERROR = "非法的json字符串";

	public static final String NO_SUCH_BLOCK = "没有该区块信息";

	public static final String NO_SUCH_TX = "没有该交易信息";

	//token info
	public static final int TOKEN_EXPIRES = 60 * 60 * 24 * 7;//TOKEN过期时间，单位秒

}
