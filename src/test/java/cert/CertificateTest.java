package cert;

import org.bouncycastle.util.encoders.Hex;
import org.springframework.util.StringUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class CertificateTest {
    private static final String STORE_PASS = "oxchains";
    private static final String ALIAS = "idcode";
    private static final String KEYSTORE_PATH = "/root/oxchains/keytools/idcode.keystore";
    private static final String CERT_PATH = "/root/oxchains/keytools/idcode.cer";
    private static final String CERT_TYPE = "X.509";

    public static void main(String[] args) throws IOException {
        //例如增加catalog，json字符串为：{"description":"description","model":"model","name":"oxchains"}
        String jsonString = "{\"description\":\"description\",\"model\":\"model\",\"name\":\"oxchains\"}";

        String signature = getSignatureString(jsonString);

        System.out.println("plaintext :     " + jsonString);
        System.out.println("Signature is :  " + signature);
        System.out.println("verify result : " + verify(jsonString.getBytes("UTF-8"), signature));
    }

    //客户端生成签名
    public static String getSignatureString(String jsonString) throws IOException {
        KeyStore keyStore = getKeyStore(STORE_PASS, KEYSTORE_PATH);
        PrivateKey privateKey = getPrivateKey(keyStore, ALIAS, STORE_PASS);
        X509Certificate certificate = getCertificateByKeystore(keyStore, ALIAS);
        byte[] signature = sign(certificate, privateKey, jsonString.getBytes("UTF-8"));
        String signatureHex = Hex.toHexString(signature).substring(0,16);//
        return signatureHex;
    }

    //服务器端验证签名
    public static boolean verify(byte[] decodedText, String receivedsignature) {
        try {
            X509Certificate certificate = getCertificateByCertPath(CERT_PATH, CERT_TYPE);
            KeyStore keyStore = getKeyStore(STORE_PASS, KEYSTORE_PATH);
            PrivateKey privateKey = getPrivateKey(keyStore, ALIAS, STORE_PASS);
            byte[] signature = sign(certificate, privateKey, decodedText);
            String signatureHex = Hex.toHexString(signature).substring(0,16);
            if(!StringUtils.isEmpty(signatureHex) && signatureHex.equals(receivedsignature)){
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static KeyStore getKeyStore(String storepass, String keystorePath)
            throws IOException {
        InputStream inputStream = null;
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            inputStream = new FileInputStream(keystorePath);
            keyStore.load(inputStream, storepass.toCharArray());
            return keyStore;
        } catch (KeyStoreException | NoSuchAlgorithmException
                | CertificateException | IOException e) {
            e.printStackTrace();
        } finally {
            if (null != inputStream) {
                inputStream.close();
            }
        }
        return null;
    }

    public static PrivateKey getPrivateKey(KeyStore keyStore, String alias,
                                           String password) {
        try {
            return (PrivateKey) keyStore.getKey(alias, password.toCharArray());
        } catch (UnrecoverableKeyException | KeyStoreException
                | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static X509Certificate getCertificateByKeystore(KeyStore keyStore,
                                                           String alias) {
        try {
            return (X509Certificate) keyStore.getCertificate(alias);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static X509Certificate getCertificateByCertPath(String path,
                                                           String certType) throws IOException {
        InputStream inputStream = null;
        try {
            CertificateFactory factory = CertificateFactory
                    .getInstance(certType);
            inputStream = new FileInputStream(path);
            Certificate certificate = factory.generateCertificate(inputStream);
            return (X509Certificate) certificate;
        } catch (CertificateException | IOException e) {
            e.printStackTrace();
        } finally {
            if (null != inputStream) {
                inputStream.close();
            }
        }
        return null;

    }

    public static byte[] sign(X509Certificate certificate,
                              PrivateKey privateKey, byte[] plainText) {
        try {
            Signature signature = Signature.getInstance(certificate
                    .getSigAlgName());
            signature.initSign(privateKey);
            signature.update(plainText);
            return signature.sign();
        } catch (NoSuchAlgorithmException | InvalidKeyException
                | SignatureException e) {
            e.printStackTrace();
        }

        return null;
    }



}