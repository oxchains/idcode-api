package com.oxchains.service;

import com.google.protobuf.InvalidProtocolBufferException;
import com.oxchains.bean.model.BlockchainInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.hyperledger.fabric.sdk.BlockInfo;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
public class BlockService extends BaseService{

    @Resource
    private FabricService fabricService;

    public BlockchainInfo queryBlockchainInfoByHashcode(String hashcode) throws InvalidArgumentException, ProposalException, DecoderException {
        return fabricService.queryBlockchainInfoByHashcode(hashcode);
    }

    public BlockchainInfo queryBlockchainInfoByBlockNum(String blocknum) throws InvalidArgumentException, ProposalException, DecoderException, InvalidProtocolBufferException {
        return fabricService.queryBlockchainInfoByBlockNum(blocknum);
    }

    public String queryTxInfoByTxId(String txId) throws InvalidArgumentException, ProposalException, InvalidProtocolBufferException {
        return fabricService.queryTransactionInfoByTxId(txId);
    }
}
